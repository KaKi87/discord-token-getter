const { app, BrowserWindow, session } = require('electron');
app.once('ready', async () => {
    const window = new BrowserWindow({ show: false });
    window.setMenuBarVisibility(false);
    window.webContents.on('did-finish-load', () => window.show());
    const { debugger: debug } = window.webContents;
    debug.attach();
    debug.on('message', async (event, method, { requestId }) => {
        if(method !== 'Network.responseReceived') return;
        try {
            const { body } = await debug.sendCommand('Network.getResponseBody', { requestId });
            const { token } = JSON.parse(body);
            if(token){
                await window.loadURL(`data:text/html;charset=utf-8,<body style="background-color: rgb(44, 47, 51)"><span style="color: white"><span style="font-family: sans-serif">${token}</span></span></body>`);
            }
        } catch(_){}
    });
    debug.sendCommand('Network.enable').then();
    await window.loadURL('https://discord.com/login');
});
app.on('quit', () => session.defaultSession.clearStorageData());